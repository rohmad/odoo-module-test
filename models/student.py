# -*- coding: utf-8 -*-

from odoo import models, fields


class SchoolStudent(models.Model):
    _name = 'school.student'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Student Table'

    name = fields.Char(string="Name", required=True)
    age = fields.Integer(string="Age")
    notes = fields.Text(string="Notes")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
    ], string="Gender", default='male')
    image = fields.Binary(string="Image")
